jQuery(document).ready(function($) {

    // Update ajax url to fix cross domain problem
    var currenturl = document.location + "";
    var hashignore = false;
    var lock = false;
    if(currenturl.indexOf('http://www.') && !ajaxurl.indexOf('http://www.')) ajaxurl = ajaxurl.replace('http://www.', 'http://');
    if(!currenturl.indexOf('http://www.') && ajaxurl.indexOf('http://www.')) ajaxurl = ajaxurl.replace('http://', 'http://www.');

	bindHandlers();
	function bindHandlers() {
		$.each($('a[href^="index.php?mpage"]'), function(idx, value){
			var obj = $(value);
			var parts = obj.attr('href').split('?');
			var link = parts[1].replace('mpage=', '');
			if(link != null) {
				obj.attr('href', '#').attr('data-link', link).click(linkAction);
			}
		});
        $('[data-link]').click(linkAction);
	}

    $(window).bind('hashchange', function(e) {
        if(!hashignore) {
            var page = document.location.hash.substring(1) == '' ? 'home' : document.location.hash.substring(1);
			if(page == 'header') return;
            pageInit(page);
            return;
        }
        hashignore = false;
    });

    // Window transistion
    function windowFade(win1, win2, ready) {
        win1.fadeOut(800, function() {
        win2.fadeIn(800, function() { $('#wrapper').trigger('mobilePageLoaded'); });
        ready.call(this);
    });
}

function pageInit(page) {
    $('.mp-content-wrap').after('<div class="mp-content-tmp" style="display:none">test</div>');
                    var simulate_part = simulate ? '&mobilepro' : '';
					$('.mp-content-tmp').load(ajaxurl + '?mpage=' + page + simulate_part, {}, function() {
                        hashignore = true;
                        window.location.hash = page;
                        windowFade($('.mp-content-wrap'), $('.mp-content-tmp'), function() {
							$('.mp-content-wrap').remove();
							bindHandlers();
                            $('body').trigger('mpage-loaded');
							lock = false;
                            $('html, body').animate({scrollTop:0}, 'slow');
                            $('#wrapper').trigger('mobilePageLoaded');
							$('.mp-content-tmp').removeClass('mp-content-tmp').addClass('mp-content-wrap');
						});
					});
}

    function linkAction(e) {
					e.stopImmediatePropagation();
		            e.preventDefault();
		            e.stopPropagation();
					if(lock) return;
					lock = true;
                    $(this).addClass('loading');
                    var page = $(this).attr('data-link');
					pageInit(page);
					return false;
    }
	
});

//Hide url bar on iphone when loaded
window.onload = function() {
	setTimeout(function() { window.scrollTo(0, 1); }, 1);
}

